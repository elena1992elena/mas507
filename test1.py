from src import math3d
import numpy as np 
from matplotlib import pyplot as plt

q=0.785398
a1 = 0.15
a2 = 0.15
# Forward kinematics
def forward(q, a1, a2):
# Static DH Parameters

    # DH Transformations    
    T01 = math3d.DH(q[0], 0, a1, 0)    
    T12 = math3d.DH(q[1], 0, a2, 0)

    # Calculate T02    
    T02 = T01.dot(T12)    
    # Get tool position p = [x, y]    
    p = T02[0:2,3]
    return p

# Inverse kinematics
def inverse(p, a1, a2, conf):

    # Read x and y    
    conf = 1
    x = p[0]    
    y = p[1]

    # Solve inverse kinematics problem based on two-link manipulator in Spong    
    D = (x**2 + y**2 - a1**2 - a2**2)/(2*a1*a2)
    theta2 = np.arctan2(conf*np.sqrt(1-D**2), D)
    theta1 = np.arctan2(y, x) - np.arctan2(a2*np.sin(theta2), a1 + a2*np.cos(theta2))
    q = np.array([theta1, theta2])

    return q

a1 = 0.15
a2 = 0.15
# Path settings
A = 0.15
x0 = 0.0
y0 = 0.0

# Create time vector
dt = 0.01
t = np.arange(0, 2*np.pi, dt)
# Prepare zero vectors
p1 = np.zeros([2,len(t)])
p2 = np.zeros([2,len(t)])
q1 = np.zeros([2,len(t)])

# Simulate IK and FK
for i in range(0, len(t)):
    # Calculate p1(t)
    p1[:,i] = [x0 + A*np.sin(t[i]), y0 + A*np.cos(t[i])]

    # Solve for joint angels q1(t) using IK
    q1[:,i] = inverse(p1[:,i], a1, a2, -1)

    # Solve for p2(t) using FK
    p2[:,i] = forward(q1[:,i], a1, a2)

# Plot path for comparison of p1(t) and p2(t)
plt.plot(p1[0,:], p1[1,:], 'b', label='p1(t)')
plt.plot(p2[0,:], p2[1,:], 'r--', label='p2(t)')
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.legend()
plt.show()

# Plot joint q1(t)
plt.plot(t, q1[0,:]/np.pi*180, 'b')
plt.plot(t, q1[1,:]/np.pi*180, 'r')
plt.xlabel('q1 [deg]')
plt.ylabel('t [s]')
plt.show()