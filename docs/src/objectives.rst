##########
Objectives
##########




The objective of this project is to design a prototype of a robot to pick the strawberries. 
The robot should locate strawberries in a strawberry farm and pick them up autonomously. T
he robot will not be designed entirely from scratch. The base for the robot is given as an open-source robot called Jetbot. Jetbot is equipped, amount other things, with a camera. 
Robot should detect the color of strawberry and pick only ripe ones.  The gripper arm and basket for strawberries should be designed and 3D printed. 
Also the necessary programming was up to each group to solve. It should be point out that, the focus is on the process of creating the robot prototype, rather than the final prototype.

For testing the final prototype model, the lecturers defined a strawberry field, as shown in figure under, which has: 

- 3 ArUco markers (for define the main path and turn points)
- 5 plants
- 8 red strawberries and 2 green strawberries 


.. figure:: images/field.JPG
   :scale: 100 %
   :alt: Strawberry Field
   :align: center

   Strawberry Field

Plastic strawberries will hang from artificial bushes using magnets. There will be a few centimeters between the base of a plant, and the edge of a berry. 
The Jetbot has to pick only the red strawberries and the time from touching the first berry until the last berry should be less than 3 minutes.

In overall, each group was supposed to learn:

1. Learn about product development methods and apply the methods in a practical project.
2. The practical project is to make a robot for harvesting strawberries autonomously.
3. Use knowledge from product development and mechatronics in practice to realize a final product prototype.
4. Verify the developed robot in a reduced scaled strawberry field.
5. Use teamwork and project management tools
