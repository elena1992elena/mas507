############
Introduction
############

The agriculture sector is changing due to the use of new technologies such as automation, 
providing significant benefits to the farmers. The strawberry farmers around the world face 
serious problems of labor shortage, due to the tedious working conditions and the general social and financial conditions. 
Today, the growth of strawberries in table-top cultivation is very common, something that facilitates the robotic harvesting 
procеssеs the berries аre more аpproachable and they differentiate from the leaves. In this way, harvesting robots offer quality, 
higher productivity, as they can operate during the whole day, and more profits to the producers without having to modify the layout 
and the size of their cultivation. The actions that take place in harvesting is the detection, the approach, the grasp and the 
placement of the strawberry in a little box. In order to automate this procedure, these actions must be accomplished by a robotic system, 
which should contain at least a computer vision system for the localization of the mature fruit, a manipulator for the movement 
of the gripper towards the grasping of the fruit. This project deals with develop a robotic solution and the automation in the harvest of strawberries, 
one of the most popular and profitable berries.


.. figure:: ./images/walle.jpg
    :width: 100%
    :align: center

[Fig]_
 

.. [Fig] Photo by Jason Leung on Unsplash


