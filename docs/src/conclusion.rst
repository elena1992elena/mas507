##########
Conclusion
##########

As a result of this project strawberry picking robot were designed. Different project management tools were used during the project, among other things Trello Project Management. 
Robot can detect red strawberry, detect Aruco Markers, measure distance to them, turn and pick strawberries. An algorithm for picking strawberries has been created.
Robot can also be controlled with joysticks, it will be demonstrated on the presentation. 


.. figure:: images/FinalDesign.png
    :scale: 80 %
    :alt: Final Design
    :align: center

