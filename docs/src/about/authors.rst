#######
Authors
#######

Project is created by the Mechatronics master students:

* Lars Bleie Andersen <larand86@gmail.com>
* Sepehr Bapiri <sepehr.bapiri100@gmail.com>
* Olena Chynchenko <elena1992elena@hotmail.com>
* Magnus Nilsen <magnusnilsenrandaberg@hotmail.com>

University of Agder, 2020
Faculty of Engineering and Science
Department of Engineering Sciences

