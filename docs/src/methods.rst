.. include:: .special.rst

.. raw:: html

    <style> .red {color:red} </style>
    <style> .green {color:green} </style>
    <style> .yellow {color:yellow} </style>
    <style> .blue {color:blue} </style>

.. role:: red
.. role:: green
.. role:: yellow
.. role:: blue


#######
Methods
#######

******************
Camera Calibration
******************

Two calibration attempts were done with similar approach. A 4x6 checkered image was photographed in different orientations by executing ``rostopic pub saveImage std_msgs/Bool``  with the ROS package running, so that the ImageSaving node was active. Upon executing, the images are saved to the designated folder
``~/catkin_ws/src/mas507/calibration/images``, and then the command was terminated after and these steps repeated until the desired amount of images were captured.    

The first calibration attempt resulted in slight edge distortion. To fix this, the new calibration was done by diversifying the 
planar orientation of the checkered picture in relation to the lens, in addition to having pictures of the pattern in all quadrants of the camera. Thus, the final calibration 
had no observable distortion. 

.. figure:: images/Fotoram.io.png
   :scale: 50 %
   :alt: Camera calibration process
   :align: center

   Camera calibration process

The 'images' folder will automatically reset when the program is relaunched, and both calibration 'libraries' were saved in new folders. The calibration 
itself is done with OpenCV, where the folder location must be specified in ``~/catkin_ws/src/mas507/calibration/calibrateCamera.py``.
The aforementioned python script is  then executed within ``~/catkin_ws/src/mas507/calibration`` to generate a ``.npz`` file that the ImageProcessing node uses to calibrate the data 
of the captured images. The WebWiz application is used to inspect the results, visiting https://jetbot-desktop1:8080 on the laptop  currently connected to the Jetbot.

Figure below shows camera image before calibration.

.. figure:: images/ImageRaw.png
   :scale: 81 %
   :alt: Before Calibration
   :align: center

After calibration (photo taken from the same place):

.. figure:: images/ImageCalibr.png
   :scale: 80 %
   :alt: After Calibration
   :align: center


**********************
ArUco Marker Detection
**********************

To navigate the Jetbot within a particular area without hard coding a specific path, the group used ArUco markers. ArUco markers are a type of binary square fiducial markers consisting of a binary matrix surrounded by a black border. Each binary matrix is a code that corresponds to an index within the ArUco library used. A dictionary of 16-bit (4x4) markers from a library of 50 unique ArUco codes was used in this project. There are two general steps in the ArUco marker detection process; detecting the markers and determining if they are indeed ArUco markers. The first steps involve thresholding, segmentation, and contour detection to find the rectangular shapes within the matrix. The second step analyzes the marker bits, determining which bits are white and which are black. Then it is determined if the identified marker belongs to the library specified in the script. 

Three ArUco markers were printed out and mounted on picture frames. The frames were then positioned at the strawberry patch's corners, leaving enough space for the Jetbot to maneuver in between. To detect the ArUco markers with the Jetbot’s camera, the function ``aruco.detectMarkers``, was used.
It uses the captured frame as input and compares it to the ArUco dictionary imported with the ``aruco.Dictionary-get`` function and the parameters created with ``aruco.DetectorParameters-create``.
When the ArUco marker is identified, the pose is estimated with the ``aruco.estimatePoseSingleMarkers`` function, which transforms the 3D coordinate system of the marker to the coordinate system of the camera. For this to work, the camera's calibration matrix and distortion vector must be provided as input arguments.
A function to draw out the 3D-coordinate axes, ``aruco.drawAxis`` , was implemented to make it possible to verify visually that the ArUco markers were adequately identified, as 
seen in figure below :red:`!!!IMAGE!!!`
The output of function was also used in a for-loop to extract the information needed to calculate the magnitude of the vector between the Jetbot and the ArUco 
coordinate systems and the Jetbot’s position in the x-direction 
relative to this vector. This was used to get the Jetbot to drive towards each successive ArUco marker, executing the strawberry picking task between each marker.

Figure below shows what does Aruco marker detection looks like on the camera.

.. figure:: images/ImageAruco.png
   :scale: 80 %
   :alt: Aruco Detection
   :align: center

********************
Strawberry Detection
********************

For detecting the strawberries, the group use the code written by the lecturer, Dr. Sondre Sanden Tørdal. The following steps explain the logic behind the code: 

- Converting ROS image messages to OpenCV images, Using ``cvBridge.imgmsg-to-cv2()``.
- Convert BGR (blue, gereen, red) to RGB (red, green, blue), using ``cvtColor(BGR2RGB)``.
- Image Blurring (Image Smoothing), using ``cv2.GaussianBlur()``. The width and height of the kernel should be specified which should be positive and odd. Also standard deviation in the X and Y directions should be specified , sigmaX and sigmaY respectively. If only sigmaX is specified, sigmaY is taken as equal to sigmaX. If both are given as zeros, they are calculated from the kernel size. Gaussian filtering is highly effective in removing Gaussian noise from the image.
- Covert RGB to HSV (hue, saturation, value), using ``cvtColor(RGB2HSV)``.
- Set the range of HSV values, using ``cv.inRange()``.
- Create elliptical/circular shaped kernels, using ``cv2.getStructuringElement()``.
- Fill small gaps and closing small holes inside the foreground objects, using ``cv2.morphologyEx()``.
- Calculate moments of binary image, using ``cv2.moments()``.
- Fit ellipse to biggest red countour detected, using ``cv2.fitEllipse()``.
- Draw a circle around the center of mass, using ``cv2.circle()``.
- Estimate pose, using ``cv2.solvePnP()``.

Good lighting is essential to detect strawberries. When using a warm light, the camera cannot detect strawberries, because red tones are dominant in the image. That's can be seen on picture below. 

.. figure:: images/BadLight.png
   :scale: 80 %
   :alt: Bad Light
   :align: center

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/dPqo2qpfvMg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


This problem disappears when using cold light lamps.

.. figure:: images/GoodLight.png
   :scale: 80 %
   :alt: Good Light
   :align: center

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/ow4TBgB2GLk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
|
**********************************
Servo Motor Calibration for Wheels
**********************************

Theoretically, the servo can be controlled between 204 (maximum backward) and 408 (maximum forward), and the theoretical stand-still center is 306. But in practice, 
the zero actuation point without shutting the robot down was measured to 319 for the left wheel and 322 for the right wheel.

*******************
Pick the Strawberry
*******************

In order to pick the strawberry with the proposed gripper, the following actions should take place:

1. Approach the strawberry with the manipulator to the coordinates given by the vision system.

2. Grasp the fruit by applying even distribution of force.

3. Rotate the wrist of the robot at the correct angle and retract.

**********
Kinematics
**********

The kinematics describes the relationship between the angle of the arm and the position in Cartesian coordinates. A commonly used convention for selecting frames of 
reference in robotics applications is the Denavit and Hartenberg (D–H) convention, which associated with Denavit–Hartenberg parameters (also called DH parameters). 
In this convention, coordinate frames are attached to the joints between two links such that one transformation is associated with the joint, [Z], and the second is 
associated with the link [X]. The coordinate transformations locating the end-link [T] along a serial robot consisting of *n* links form the kinematics equations 
of the robot are shown in Equation 1:

.. figure:: images/e1.png
   :scale: 100 %
   :alt: equations
   :align: center

The Denavit-Hartenberg parameter tables consist of four variables:

- The two variables used for rotation are *θ* and *α*
- The two variables used for displacement are *r* and *d*

As shown in figure below, We have two coordinate frames here, so we need to have one rows in our D-H table (i.e. number of rows = number of frames -1). 
For the Joint 1 (Servo 0) row, we are going to focus on the relationship between frame 0 and frame 1. The Following steps shows the procedure to find the H-D parameters:  

.. figure:: images/side_view.JPG
   :scale: 100 %
   :alt: X-Y View of Basket and Arm
   :align: center

   X-Y View of Basket and Arm

- *θ* is the angle from *X0* to *X1* around *Z0*. *X0* and $X_1$ both point in the same direction. The axes are therefore aligned. When the robot is in motion, *θ* will change (which will cause frame 1 to move relative to frame 0). The angle from *X0* to *X1* around *Z0* will be *θ*. 
- *α* is the angle from *Z0* to *Z1* around *X1*. According to figure above, no matter what happens to *θ*, the angle from *Z0* to *Z1* will be 0 (since both axes point in the same direction).
- To find *r*, the distance between the origin of frame 0 and the origin of frame 1 along the *X1* direction should be measured. As figure indicates, *r* is the length of the Jetbot's arm which is 180 mm.
- *d* is the distance from *X0* to *X1* along the *Z0* direction. As can be seen, this distance is 0.

Once the Denavit-Hartenberg (D-H) parameters was found and  the table 1 was created, the homogeneous transformation matrix *Tn* is calculated by using the D-H parameters as shown in Equation 2:

.. figure:: images/table1.png
   :scale: 100 %
   :alt: Table 1
   :align: center

As can be seen from Equation 2, the kinematics is described in one plane since the robotic manipulator is only operating with one degree of freedom. 
The X, Y and Z position can be seen in the right cоlumn in the $T_n$ matrix. The position will be equivalent to the pоsition on the pеrіphery of a cіrcle, making sense since it is
just an arm with one joint. Therefore the inverse kinematics is relatively simple and can beseen in the Equation 3:

.. figure:: images/Eq3.png
   :scale: 100 %
   :alt: Eq3
   :align: center

*****************
Control of Jetbot
*****************

NB! Complete code can be found in :ref:`Attachments` and on the GitLab: https://gitlab.com/elena1992elena/mas507

``ArucoPosition.msg`` was created. This message contains coordinates of Aruco Markers which are obtained from ``JetbotCamera.py``. This messege file was registrated in the ``CMakeList.txt`` (line 53). To let ROS knew about ``ArucoPosition.msg``, ``mas507 package`` was recompiled. 
Custom publisher was created (line 130, JetbotCamera.py). 

.. figure:: images/pubA.png
   :scale: 100 %
   :alt: pub
   :align: center
 
Object ``Aruco_Position`` was created (line 175, JetbotCamera.py) and the following parameters have been set: Aruco Marker ID, x, y, and z coordinates. Coordinates are multiplied by 1000 to convert into millimeters. This Aruco Marker position is published using publisher.

.. figure:: images/ObjA.png
   :scale: 74 %
   :alt: pub
   :align: center

Subscribing to ivent

.. figure:: images/sub.png
   :scale: 100 %
   :alt: sub
   :align: center

Than array for 10 markers was created (line 21, step1.py). Depending of Aruco Marker ID it writes to the corresponding position into array. 
Than while loop checks the distance to an Aruco Marker.

.. figure:: images/whileA.png
   :scale: 100 %
   :alt: while
   :align: center

