##########
Discussion
##########

The automation of strawberry picking pursued in this report using the  methods described serves as a useful primer for the powerful tools utilized. 
Through GitLab, collaboration in both programming and subsequent report writing is done more effectively, especially with multiple collaborators. 
Visual Studio Code serves as an intuitive programming environment for using both OpenCV, ROS and Python, which are compatible. This constitutes the open-source, 
free to use software tools this project was realized through. 

The strawberry picking in question is a very future oriented pursuit, aimed towards concepts such as vertical farms and a much larger degree of automation to improve farm efficacy. With viable farmland requiring the decimation of forests, aforementioned concepts grow ever more relevant and necessary. The United Nations 'Sustainable Development Goals' go hand in hand with economically feasible and environmentally sustainable automation. There are many problems that require solutions for such methods to truly be sustainable, but any foray into such solutions could be the stepping stone needed towards true sustainability. While increased automation often brings to mind the association of workers being replaced, which is categorically true, it does not mean that less has jobs are available. This project also came with the lesson of how challenging effective automation can be. Increased automation would require increased qualified workforce to project, implement and maintain such systems. This ties back into the UN goals, namely 8, 9 and 10.
The strawberry 'farm' used in this project is simplified , and nominal strawberry location has low deviance which is unrealistic. This could introduce automation problems as strawberries growing in wildly different locations on a real bush could require grippers with up to six degrees of freedom. The gripper designed in this project has 3 DOF in comparison, as the strawberries are mostly in the  same place for each bush, requiring less versatile harvesting methods.

.. figure:: images/unsplash.jpg
    :width: 110%
    :alt: strawberries
    :align: center