
###########
Attachments
###########

.. figure:: images/ArmDr.png
    :width: 100%
    :alt: Arm Dr
    :align: center

    Arm drawing

.. figure:: images/BasketDr.png
    :width: 100%
    :alt: Basket Dr
    :align: center

    Basket drawing

.. figure:: images/TopPlate.png
    :width: 100%
    :alt: Plate
    :align: center

The complete project can be found on GitLab: 
https://gitlab.com/elena1992elena/mas507



Algorithm of robot movements. File ``step1.py``.


.. literalinclude:: ../../src/step1.py
    :language: python
    :linenos:


Updated ``JetbotCamera.py``:

.. literalinclude:: ../../src/JetbotCamera.py
    :language: python
    :linenos: