###################
Concept Development
###################

Design of the product is essential for productive use. A successful design will reduce errors, will help to decrease cycle time of strawberry picking, 
increase throughput, improve system reliability, compensate for robot inaccuracy. That's why each part should be designed carefully. 
Design of the strawberry collector robot can be divided into three main parts:

- design of the gripper
- design of the arm
- design of the basket.


*****************************
Design of the gripper and the arm
*****************************

The following criteria were considered for the design of the gripper arm:

- Must be able to adjust the height of the gripper
- Must be able to pick a strawberry and hold it
- Must be able to drop a strawberry into a basket
- Must not block the camera's field of vision
- Center of gravity on the robot as a whole must be considered


First concept for gripper
=========================

The first idea for the gripper that was considered by the group, was a standard gripper with one motor for roll-movement and one motor for the gripping. 

.. figure:: images/gripper1.JPG
   :width: 5cm
   :alt: Gripper with servo
   :align: center

   Gripper, concept 1

Second concept for gripper
==========================

The second idea was a gripper made from a box. This box gripper would give us at least one free motor gained from the gripping task, but it might still need a motor for the pitch to drop the strawberry into a basket, depending on the design of the arm. The procedure is thought to be done by first picking the strawberry by threading the strawberry stem through the hole in the top. The catching/holding of the strawberry is also done by the box, by a tilt in the construction. This is also a more realistic option for the gripper, considering that a real strawberry would have to be handled with care and not be squeezed by the gripper.

.. figure:: ./images/gripper2.JPG 
   :width: 5cm
   :alt: Gripper 
   :align: center

   Gripper, concept 2


First concept for arm
=====================

The first concept for the arm was an arm with a motor at its base for the yaw-movement, and a mechanically actuated joint at the middle of the arm. It would be a good choice for the first gripper, due to it only needing one motor for the yaw of the base of the arm. It is then hypothesized to be mounted at the center of gravity, considering the weight of the three motors required for actuation. This arm concept would also be a possible choice for the the second gripper, regarding that there are two motors available. One motor could then be used to control the pitch of the gripper box and the other one to control the yaw of the base of the gripper. The calculations and construction seems complex, at least when considering the criteria of not blocking the field of vision for the camera.

.. figure:: images/arm1.JPG
   :width: 10cm
   :alt: Arm 1 
   :align: center

   Arm, concept 1 

Second concept for arm
======================
The second concept for the arm does not work with the first concept for the gripper, but is a better fit for for the second concept. 
In this design gripper and arm is an one part. That will reduce number of movable parts, which in turn reduces the number of possible breakdowns. It only requires one motor for the pitch of the arm, 
and would complement the box gripper perfectly with its ability to control both the pulling of the stem and the sliding of the strawberries into a basket. It would be the solution that would be easiest to not interfere with the camera's field of vision, and also be the option with the least amount of weight due to the size of the arm and number of motors required.


.. figure:: images/arm2.JPG
    :width: 10cm
    :alt: Arm 2
    :align: center  

    Arm, concept 2

Selection of concept for gripper arm
====================================

The group decided that it would try to make the construction as mechanically actuated as possible, 
instead of designing electrical actuation for the every individual joint in the construction. The second gripper concept seemed to complex to implement at first, but after looking at similar examples online, 
the group decided that this would be the best solution for our project. The second concept for arm is the most economical solution in terms of number of actuators, and should be the most simple to control. 
The final choice of concept then satisfies the mentioned criteria. Due to the light weight of the 3D-printed gripper arm, it will not affect the center of gravity in a noticeable way. 
The width, length and height of the gripper box were calculated by considering the strawberries as squares and their largest diameter as the width and height.

- Strawberry length without stem   -  3.5 cm
- Strawberry diameter   -   3.5 cm


********************
Design of the basket
********************

Following criteria were considered for design of the basket:

- Must be large enough to carry 8 strawberries
- Should be easy to access for the arm (e.g. have an opening if necessary)

The basket was designed after the concept for gripper arm had been selected. 
The group then already knew thatthe gripper arm would be connected in the front end of the basket, as the best way to collect the strawberrieswith the chosen gripper arm concept.
The design of the basket was therefore mostly dependant on the considerations for placement and the dimensions for size in order to have the ability to store at least 8 strawberries. 


.. figure:: images/Basket1.png
    :scale: 50 %
    :alt: Basket 1
    :align: center 

    Basket concept


*****************************
Placing of the basket and the arm
*****************************

Two different positions of the basket and arm was considered. 
The basket could either be placed above the heat sink, or it could be placed in the front of the Jetbot. 

.. figure:: images/Arm2Top.png
    :scale: 50 %
    :alt: Arm top
    :align: center 

.. figure:: images/Arm2Front.png
    :scale: 50 %
    :alt: Arm front
    :align: center
    

Placing the basket at the top of the heat sink would enable the positioning of the servomotor to be closer to the center of gravity. 
If it instead would be placed in front of the Jetbot, it can cause the robot to be less stable. Also robot will be less maneuverable, beacuse of it length.

***********
Radar chart
***********

Figure below shows a radar chart to evaluate the different concepts for arm and gripper of the robot along with a weighted score on a scale 
from 1-10 for each criterion is presented. A higher score indicates better performance. A high score in one category does mean that it is the best choice overall. 
The weight factor indicates how much each criterion is emphasized in this category and how vital that particular category is to the product as a whole.


.. figure:: images/radardiagram.png
    :scale: 120%
    :alt: Criteria of Concepts choice
    :align: center

    Criteria of Concepts choice

*****************************
Determining the final concept
*****************************
Based on radar chart it was decided to develop the second arm concept with integrated gripper further. Basket position above the heat sink seemed to be more reliable because of the center of gravity. That's why this basket lockation was chosen for the final concept.

After these decisions have been made, concept went through several design cycles. First prototype was printed on 3D printer.

.. figure:: images/Print1.jpg
    :width: 12cm
    :alt: Print 1
    :align: center

.. figure:: images/Basket1.jpg
    :width: 12cm
    :alt: Basket 1
    :align: center



Issues that were discovered:

- Both the basket and the arm were printed with small holes on the bottom plate due to reduce weight. This solution turned out to be very time consuming to produce. It takes long time for 3D printer to print small details. That's why those holes was removed in the next prototype. 
- Basket size is too small for 8 berries.
- The width of the arm is a bit small for a strawberry to fall into the basket.
- The cut for the strawberry stem is short and it could be bettter with multiple cuts. 
- Size of gripper should be bigger.

These issues have been taken into account and the design has been improved.

The servomotor was moved inside the basket, also it was flipped from horizontal to vertical position, which helped to increase basket volume. The height of the basket walls has also been increased. 


.. figure:: images/Basket2.png
    :scale: 50 %
    :alt: Basket 2
    :align: center 

    Basket, concept 2    

The arm was also updated to improve issues. Geometri of gripper part was chenget to improve the capture efficiency. After testing in was decided that 20% angle would be the best choice.

|pic1|  |pic2|  |pic3|

.. |pic1| image:: images/GripperFlat.png
   :width: 30%

.. |pic2| image:: images/Gripper20.png
   :width: 30%

.. |pic3| image:: images/Gripper25.png
   :width: 30%


Also cuts for the strawberry stems where added. 

.. figure:: images/GripperH.png
    :width: 30%
    :alt: Final Design
    :align: center


Drawings for the final design can be found in :ref:`Attachments`.
Final design concept shown on figure under:

.. figure:: images/FinalDesign.png
    :scale: 50 %
    :alt: Final Design
    :align: center

    Final Design

After printing the second prototype and testing it, it was concluded that it meets all the criteria.

.. figure:: images/robot.jpg
    :scale: 50 %
    :alt: Final Design
    :align: center


.. figure:: images/robot1.jpg
    :scale: 50 %
    :alt: Final Design
    :align: center

    Final Design

    