######
Theory
######


******************
Available Hardware
******************

Frame
=====

The purpose of the frame is to mount all the components together. The chassis was provided by the lecturer at the start of the project, and consists of 3D printed PLA.


.. figure:: ./images/frame.JPG
   :scale: 100 %
   :alt: Frame of Jetbot
   :align: center

   Frame of Jetbot




Parallax Feedback 360°High-Speed Servo
======================================

The Parallax Feedback 360° , Figure \ref{fig:motor}, provides the functionality of a light-duty standard servo, a continuous rotation servo, 
a high-speed servo, and encoder feedback in one convenient package. Its speed is controlled with standard RC servo pulses, 
just like a typical continuous rotation servo, and it provides a return signal line from an internal Hall effect sensor 
that provides digital angular position feedback. The complete specifications of this servo motor are available in [1]_
 



.. figure:: ./images/motor.JPG
   :scale: 50 %
   :alt: High speed Servo motor
   :align: center

   High speed servo motor




Wheel
=====

Figure under show the wheel that has been implement in Jetbot. The wheel have silicone tires and measure 60 mm in diameter. 
Four additional mounting holes for 4-40 screws make it possible to use the wheel with several universal mounting hubs [2]_.

.. figure:: ./images/wheel.JPG
   :scale: 50 %
   :alt: Wheels
   :align: center

   Jetbot wheel


NVIDIA Jetson Nano
==================

The Jetson Nano developer kit is an AI computer developed by NVIDIA. It features a camera connector for CSI cameras, 
a 128-core NVIDIA Maxwell GPU and a quad-core ARM A57 CPU. It is particularly suitable for tasks such as image classification, object detection, segmentation and speech recognition.

.. figure:: ./images/nvidia.JPG
   :scale: 50 %
   :alt: NVIDIA Jetson Nano
   :align: center

   NVIDIA Jetson Nano

Camera
======

A wide angle camera LI-IMX219-MIPIFF- NANO-H145 V1.2 by Leopard Imaging was used to capture the video, enabling the implementation of computer vision using OpenCV [3]_. 
Figure under shows the camera that mounted on the NVIDIA Jetson Nano. 

.. figure:: ./images/camera.JPG
   :scale: 50 %
   :alt: Camera mounted on NVIDIA Jetson Nano
   :align: center

   Camera mounted on NVIDIA Jetson Nano


PCA9685 Motor Driver
====================

The driver used to control the motors is the Adafruit PCA9685, a 16-channel, 12 bit PWM servo driver [4]_. 

.. figure:: ./images/motor_drive.JPG
   :scale: 50 %
   :alt: Motor Driver
   :align: center

   Motor Driver


Buck Converter
==============

A 300W XL4016 DC-DC Max 9A Step Down Buck Converter, to get a stable, appropriately high voltage was used. A complete specification of the converter is available in the link below [5]_

.. figure:: ./images/buck.JPG
   :scale: 50 %
   :alt: Buck Converter
   :align: center

   Buck Converter

JX Servo Motor
==============

The maximum of three 17g Metal Gear Core Motor Micro Digital Servo with 180 degree motion used for designing the arm of Jetbot [6]_.

.. figure:: ./images/jx_servo.JPG
   :scale: 50 %
   :alt: JX PDI-1171MG Servo Motor
   :align: center

   JX PDI-1171MG Servo Motor


Caster Ball
===========

An omnidirectional caster ball as shown in is mounted at the back of the Jetbot, in order to keep balance. A caster ball is advantageous over a caster wheel because a caster wheel can have problems with sudden changes in the orientation.

.. figure:: ./images/caster_ball.JPG
   :scale: 50 %
   :alt: Caster Ball
   :align: center

   Caster Ball


Battery
=======

The Jetbot powered by a 4000 mAh 7.4V 2-cell LiPo battery.

******************
Available Software
******************

Visual Studio Code Remote SSH Development
=========================================

The client side code editor used for this project is Visual Studio Code. It features syntax highlighting, auto-complete, and support for a multitude of programming languages. It had been developed by Microsoft for Windows, Linux, and macOS. We use this program to connect to a remote server, in this case the Jetson Nano. It was used to handle secure shell (SSH) communication with the Jetson Nano, making it possible to edit the commands on the laptop (client) and execute them on the Jetson. We added GitLab VS Code extension to help group work cohesively and effectively from the first stage of implementing to the last stage — deploying implementation to production. Figure below shows the programs environment.

.. figure:: ./images/vscode.JPG
   :scale: 70 %
   :align: center

   VS Code environment



OpenCV
======

OpenCV is a library of programming functions used for image processing and computer vision. It is free to use, and compatible with several programming languages, such as Python and C++.

Robot Operating System (ROS)
============================

The Robot Operating System (ROS) is a flexible framework for writing robot software. It is a collection of tools, libraries, and conventions that aim to simplify the task of creating complex and robust robot behavior across a wide variety of robotic platforms [7]_.

Python
======

To programming the Jetbot and using OpenCV, Python (version 2.7) was used. Python is a high level, general purpose programming language. Python has a simple syntax and an extensive standard library of functions, making it suitable for engineering and scientific purposes.

.. [1] Parallax feedback 360°high-speed servo https://www.pololu.com/product/3432
.. [2] Wheels https://www.aliexpress.com/item/32609379555.html?spm=2114.search0604.3.125.446f3bf1kTm6ZZ&ws_ab_test=searchweb0_0,searchweb201602_1_10065_10068_319_10546_10059_10884_317_10548_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_60,ppcSwitch_0&algo_expid=9898ba5d-178a-4685-9130-808fa663b922-16&algo_pvid=9898ba5d-178a-4685-9130-808fa663b922
.. [3] Camera https://https//www.leopardimaging.com/product/nvidia-jetson-cameras/nvidia_nano_mipi_camera_kits/li-imx219-mipi-ff-nano/li-imx219-mipi-ff-nano-h136/
.. [4] Motor driver https://www.aliexpress.com/item/32469378576.html?spm=2114.search0604.3.15.33e223a5Odmqde&ws_ab_test=searchweb0_0,searchweb201602_1_10065_10068_319_10546_10059_10884_317_10548_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_60,ppcSwitch_0&algo_expid=55106b56-4314-4c4b-a2c4-f4979ec4faef-2&algo_pvid=55106b56-4314-4c4b-a2c4-f4979ec4faef
.. [5] Buck converter https://www.aliexpress.com/item/32639621677.html?spm=2114.search0604.3.8.16234ecbtyEC1Q&s=p&ws_ab_test=searchweb0_0,searchweb201602_1_10065_10068_319_10546_10059_10884_317_10548_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_60,ppcSwitch_0&algo_expid=7b88a884-e18a-408a-901e-64d22c8e01b7-1&algo_pvid=7b88a884-e18a-408a-901e-64d22c8e01b7&transAbTest=ae803_5
.. [6] Jx servo motor https://www.aliexpress.com/item/32799479932.html?spm=2114.search0604.3.302.5b36346afDVXXm&ws_ab_test=searchweb0_0,searchweb201602_1_10065_10068_319_10546_10059_10884_317_10548_10887_10696_321_322_10084_453_10083_454_10103_10618_10307_537_536,searchweb201603_60,ppcSwitch_0&algo_expid=48eae803-00ca-48eb-ba92-36e4f4437550-41&algo_pvid=48eae803-00ca-48eb-ba92-36e4f4437550&transAbTest=ae803_5
.. [7] ROS https://www.ros.org/about-ros/


