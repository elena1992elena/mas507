.. include:: .special.rst

.. raw:: html

    <style> .red {color:red} </style>
    <style> .green {color:green} </style>
    <style> .yellow {color:yellow} </style>
    <style> .blue {color:blue} </style>

.. role:: red
.. role:: green
.. role:: yellow
.. role:: blue


#######################
Project Management (PM)
#######################


**************************************************
Historical view on projects and project management
**************************************************
Projects have been implemented around the world for hundreds of years. Large project planners and executives are increasingly relying on emerging engineering science concepts and e
mphasizing the importance of timely presentation. It is possible to describe this type of project as "major technical and commercial projects involving contract systems between the 
parties" with a strong focus on problem-solving and greater efficiency by working together in corporate networks. After the more technical development projects were concerned, for example, 
the development of the telegraph and telephone, and then focus on the emphasis on planning by engineers and delivery contract, but in the 1950s, scientific approach and methodology to rise. 
This decade is considered today as the beginning of modern PM. During this period, engineers mastered the methods and techniques of project management like
cost estimates, prototype design, operating methods, supply chain management and contract negotiations. After that, in the 1970s and 1980s, the focus shifted to a more team-oriented approach, 
with research focusing on how project teams and the project manager affected project success. In the 1980s, computer tools and tracking methods were developed, and projects modeled as larger 
inventories coincided with mechatronics' advent and its increasing entry into information technology and computer science. Research on project management has advanced greatly since the 1990s, 
becoming more of an organizational state and a system for anticipating and rationalizing interim collective initiatives during the 2000s. [1]_ [2]_

*********************************************
Values and applications in project management
*********************************************

Old fashion PMs are often introduced as a set of norm procedures that is forced to result in successful projects if they are pursued. In contract, many studies and investigation indicates that, 
it is not possible to achieve the assigned goals and benefits, even using Pm procedures. Also, researchers claim that in a big and complicated projects, using traditional project management has 
the opposite of the desired effect.
Typical and traditional methods used strictly based on the principle of "manage as planned." But it is hard to understand and predict how a complex project depends on causal feedback and 
non-linear behaviour (Which also effects may be visible after significant delays). In total, project complication can be describe in two manner as follow: 

1. Structural complexity (made up of the size or number of elements in the project) and the interdependence between these elements.
2. Uncertainty both in formulating and identifying the project goals as well as how to achieve these goals. Therefore the project behaviour most often is complex аnd it’s when uncertainty affects a traditionally managed project that is structurally complex that you need systemic modelling focus [3]_.




***********************
The Essence Kernel [4]_
***********************

Before start to define the Essence Kernel, It should point out that the Essence Kernel was designed for using of software engineers, but nowadays every branch of engineering and 
team-works which need product management in their groups, can enlisting from the benefits of this PM method. Therefore, our group decided to use this method as one the tools for 
the project management during the semester. 
Essence Kernel is a collection of light and lightweight, from definitions that depict the nature of practical and scalable engineering in a practically independent way. This is a new way of looking at engineering, a new way of understanding the progress and health of development efforts, and a new way of combining methods into one effective working way. This is a standard reference model that all teams can use to inspect, adapt, and continually improve their approach continuously. Areas of concern are organized into three discrete, and each focuses on specific aspects of software engineering, and each distinguished by the use of a different colors as follow:

1. **Customer:**  This concern includes everything related to the actual use and operation of the system being produced. The area of concern and its card are colored :green:`Green`. 
In the customer area, the team must understand the stakeholders and have the opportunity to address the following:
|

   - *Stakeholders.* Team member, the hypothetical company which we make the Jetbot for them, and the university which provide the source and equipment are the stakeholders. Responsibility of key represented stakeholders should be defined. All the stakeholders have to involve into the Jetbot projects in a way that the system meets the expectations.

   - *Opportunity.*  The set of conditions that makes it appropriate to creation or modification of the system. The opportunity expresses the idea for the creation of the new, or changed, mechatronic system. It demonstrates the team's common understanding of the stakeholders' needs establishment, and provides justification for development, helps shape the requirements of the new software system.

2. **Solution:** This area, is about everything related to the specification and development of the system. This area of concern and its cards are colored :yellow:`Yellow`. In addition, the team needs to establish a shared understanding of the requirements, and implement, build, test, deploy and support a mechatronic system that fulfills them:
|
   - *Requirements.* Addressing opportunities and satisfying stakeholders is the primary goal of the system. It is important to figure out the necessary requirements and essential characteristics of mechatronic system, and use the to drive the development and testing of the new system. In the project the group tried to identify all the requirements which is vital to start the process of prototyping and developing of the codes for control the robot. 
   - *Software System (Mechatronic System).*  Mechatronic System is made up mechanical engineering, electronics, control and automation, and computer technology which leads to design, develop, put into operation and optimize systems that operate in highly dynamic conditions with high precision, robustness and multi-actions.

3. **Endeavor:** Everything that related to the the team, and the way that they approach their work are include in this Are. This area of concern is colored :blue:`Blue.` In the Endeavor, the group and the way they want to involve and work have to be formed, and the work has to be done as follow:
|
   - *Work.* Physical and mental efforts to achieve the development of a mechatronic system and finalize it is in the Work. in this part, all the tasks are identified and prioritizes and the development is started. After that, all the progress should be monitored and measured and necessary re-works established to have an acceptable results.
   - *Team.* Fist the mission defined and the group with individual responsibilities are formed. The team should works as one unit to have better performance and to finish their mission.
   - *Way of Working* The first important parameters and criteria in this part, is the principles establishment. Weaknesses and strengths of each person in the group should defined and the gaps between the the capability and limitation should be completely understood. The team develops its working method with an understanding of its mission and work environment. As the group continues to work, they are continually reflecting on their work and adapting it to their current situation if necessary.


***********
Alpha Cards
***********

According to the contents that explained before, the kernel includes seven key elements; Requirements, Software System (Mechatronic System), Team, Work, Way of Working, Opportunity and Stakeholders. 
Through states defined on these elements, the kernel provides a perceptive tool for practitioners to reason about their endeavors' progress and health in a practice-independent way. To individualize them from other products used to describe them these elements are called *Alphas*.

.. figure:: ./images/alpha_cards.jpg
   :scale: 50 %
   :alt: Illustration for using Alpha Cards during project
   :align: center


******************************
Trello Project Management [5]_
******************************

Trello is an online list-making application. It can be used to organize or prioritize the tasks, goals, notes, projects, shared files, or anything else as *Cards* that helps a team work together. It is possible to communicate in the group without using email or other communication apps, which is caused to wasting time. 
Trello provides an intuitive platform to keep progress continuous and served as an overview of project problems and the respective subsystems and parts' progress. In other words, a valuable tool when several people are undertaking a complicated task. 
By keeping all the information the team needs in one place easily puts everything on track, quickly realizes what has been done, and gets back to the previous conversations.
Also, in terms of transparency, showing what needs to be done, the status of all potential tasks, and bottlenecks are easy. Figure below shows how our group used this platform as one of project management method to keep up with daily-weekly plans and goals organized.

.. figure:: ./images/trello.JPG
   :scale: 50 %
   :alt: Trello Project Management Demonstration
   :align: center

   Trello Project Management Demonstration


*********************************************************************
Continuous Integration, Continuous Delivery and Continuous Deployment
*********************************************************************

The Internet, network infrastructure, and capable browsers have enabled cloud software to be a general approach to provide different kinds of services to users. The web-based solutions enable the collection of usage data continuously and in real-time. This data could be used to observe and analyze the system's performance or as an input for 
product development to support decision-making about the next steps in the development project. This way, developers can concentrate on implementing features that are important for users and hence reduce waste by not further implementing the features that are not used.

The Internet, network infrastructure, and powerful browsers have made cloud software a general approach to providing various services to users. Web-based solutions make it possible to collect 
usage data continuously and in real-time. This data can be used to view and analyze performance or system input for product development to support decisions about the next development project stages. 
In this way, developers can implement basic features for users and thus reduce waste by not implementing more new features. [6]_

*Continuous delivery* is about assuring that the product under development of software is ready; hence it is always ready to be placed in the production environment. The Continuous delivery contains 
the continuous integration phase, including automated deployment to testing environments for automated acceptance and performance testing. [7]_

In *Continuous Deployment* every change that passes the automated tests will be passes automatically to the production environment. All test phases should be automated in this kind 
of practice, including acceptance tests that cover functional and non-functional requirements. Successful continuous deployment depends on automated tests that are of outstanding quality. 
It is a more radical approach than continuous delivery.
In figure below there is a presentation of the differences between these three techniques. A Box describes the process to take place, and the line between the boxes is describing if the 
step between the processes is automated or not [8]_. The group used GitLab CI/CD as a powerful tool built into GitLab that allows us to apply all the continuous methods (Continuous 
Integration, Delivery, and Deployment) to our software with no third-party application or integration needed.


.. figure:: ./images/cd_ci.JPG
   :scale: 100 %
   :alt: Continuous Integration, Delivery and Deployment
   :align: center

   Continuous Integration, Delivery and Deployment


*************
Team Contract
*************

Each group has to use a team-contract to improve its performance. In the first step, each group member reflects and fill out the form base on her/his background and expectation individually. 
After that, the group members discussed their idea based on the forms and got inspiration from each other. 
After developing the final draft of the contract, the team agreed and signed the contract. This contract is a part of the agreement in *Stakeholder-In Agreement* Alpha cards, which are explained in :ref:`Alpha Cards` 

.. figure:: ./images/contract.jpg
   :scale: 20%
   :alt: Team Contract
   :align: center

   Team Contract



.. [1] K. Artto, M. Martinsuo, and J. Kujala, “Project business. Helsinki, finland, ”Project Management Association Finland, 2011.
.. [2] E. Flening, A. Jerbrant, “Worlds apart and close together:  Relating mechatronics and project management research,” in DS 92: Proceedings of the DESIGN 2018 15th International Design Conference,2018, pp. 2867–2878.
.. [3] T. Williams, “Assessing and moving on from the dominant project management discourse in the light ofproject overruns,” IEEE Transactions on engineering management, vol. 52, no. 4, pp. 497–508, 2005.
.. [4] Essence user guide, http://semat.org/quick-reference-guide
.. [5] What is trello, https://blog.hubstaff.com/trello-project-management/#what-is-trello
.. [6] J. Humble and D. Farley, Continuous delivery: reliable software releases through build, test, and deployment automation.  Pearson Education, 2010
.. [7] J. Humble, “Continuous delivery vs continuous deployment,” http://continuousdelivery.com/2010/08/continuous-delivery-vs-continuous-deployment/ , Tarkastettu, vol. 13, no. 2014, p. 8, 2010
.. [8] V. Pulkkinen, “Continuous deployment of software,” in Proc. of the Seminar, vol. 58312107, 2013, pp.46–52.




