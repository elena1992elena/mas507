#######
Results
#######

Arm and basket for strawberry picking robot were designed. After evaluating several concepts next variant was chosen:

.. figure:: images/FinalDesign.png
    :scale: 60 %
    :alt: Final Design
    :align: center



.. figure:: images/robot.jpg
    :scale: 30 %
    :alt: Final Design
    :align: center


.. figure:: images/robot1.jpg
    :scale: 35 %
    :alt: Final Design
    :align: center

    Final Design

Camera of the robot was calibrated to detect red strawberries and also to detect Aruco Markers. 


|pic1|  |pic2|

.. |pic1| image:: images/GoodLight.png
   :width: 48%

.. |pic2| image:: images/ImageAruco.png
   :width: 48%




Algorithm for the strawberry picking was created. Complete algorithm can be found in the ``step1.py`` file in :ref:`Attachments` or on GitLab https://gitlab.com/elena1992elena/mas507

Robot is able to mesure distance to Aruco Markers, detect red strawberry, turn to the strawberry and pick the strawberry. 


Strawberry field:

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/tYabbXQ9RgI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|
Arm lifting:

.. raw:: html

    <iframe width="560" height="315" src="https://www.youtube.com/embed/hcfsiTJZGCg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|
Turn to the strawberry:

.. raw:: html
    
    <iframe width="560" height="315" src="https://www.youtube.com/embed/j1FEZbJC4xY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|
Strawberry picking:

.. raw:: html
    
    <iframe width="560" height="315" src="https://www.youtube.com/embed/zwuQAbZ2oPw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>    
    

