###################
Project Development
###################

*****************
User Requirements
*****************

Although the product to be developed in this project was the autonomous strawberry picker, 
and the customers are the farmers who will use it to harvest their crops, another end product must be kept in mind, 
namely the strawberries. And the strawberries will be bought by another customer group; the strawberry consumers. Consequently, 
the quality of the strawberries that are being picked must be held to a high standard. It means that the machine must be able to 
pick selectively based on a range of criteria. The following list contains the required quality of consumer-grade strawberries according to OFG [1]_:

- Each strawberry must be dry
- Should have a shiny surface
- Have no holes
- Have no signs of pressure damage
- Be unaffected by rot or disease
- Have an even color
- The strawberries should have about the same size
- Be ripe

**************************************
UN Sustainable Development Goals (SDG)
**************************************

The Sustainable Development Goals are a call for action by all countries – poor, rich and middle-income – to promote prosperity while protecting the planet. 
They recognize that ending poverty must go hand-in-hand with strategies that build economic growth and address a range of social needs including education, health, 
social protection, and job opportunities, while tackling climate change and environmental protection [2]_.  The group discussed the relation between the designing the 
Jetbot and SDGs and how the final product could affect these goals. It is worth noting that some of the United Nation goals are not related of the scope of this projects. 
The results of the conversation are as follows:

*Goal 1:* Automating strawberry picking could speed up the production process and help the economics of poor people.

*Goal 2:* By designing an efficient we could decrease the waste of food, by picking only the optimally ripe strawberries and leaving unripe berries to grow further.

*Goal 4&5: *If the robot succeeded in making farms more effective, farmers could end up with better means to educate and send their children to school, also it caused too empowering women in poor countries. 

*Goal 7:* If efficient and cheap robots are able to make, we could have  more efficient harvesting. Therefore, less energy and resources are used. Also, batteries could be charged using renewable means.

*Goal 8:* The robots and other types of automated equipment would need maintenance which means trained engineers and technicians are required, creating job opportunities for youth.

*Goal 9:* Automated systems would require proper infrastructure for maintenance and high amount product.

*Goal 10:* Underdeveloped countries have a high percentage of the population living as farmers, and a more efficient farm model could free up their time to pursue education, or make money in other ways.

*Goal 11:* The robot would have to be \emph{recyclable} or \emph{reusable} for it to be sustainable.

*Goal 15:* Such picking technologies could allow for making more ‘dense’ farms which could be more efficient and as a result, less land is occupied.

.. [1]  “Strawberry,” https://www.frukt.no/ravarer/bar/jordbar/
.. [2] “Sustainable development goals,” https://www.un.org/sustainabledevelopment/.27
