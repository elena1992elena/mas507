########
Abstract
########

This report gives an upfront walk-through of the process of creating an automatic strawberry collector with pre-made configurations and wired hardware by the lecturer for the provided Jetbot. The project required the use of a Python-script with OpenCV kinematics for machine vision to detect ripe and unripe strawberries and the ROS middleware for parallel processing of the node communication in ROS combined with the video streaming via OpenCV. It also included the utilization of the Git control system for the contributions, storage and implementation of this project. The report was published on this website with the Sphinx documentation generator.

.. figure:: images/unsplash.jpg
    :width: 110%
    :alt: strawberries
    :align: center