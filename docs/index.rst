.. MAS507 documentation master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: images//uia.png
   :width: 600px
   :align: center
   :alt: alternate text



.. raw:: html

   <h2 style="text-align: center">University of Agder</h2>
   <p style="text-align: center">Department of Engineering Sciences<br>Grimstad, Norway</p>
   
|

.. image:: images//jetbot.jpg
   :width: 12cm
   :align: center

|
|

MAS507 - Product Development and Project Management
===================================================

Student Group 1
-------------

- Lars Bleie Andersen
- Sepehr Bapiri
- Olena Chynchenko
- Magnus Nilsen


Lecturers
---------

- Sondre Sanden Tørdal, PhD
   - Postdoctoral Fellow in Mehcatronics
   - https://www.uia.no/kk/profil/sondrest

- Mette Mo Jakobsen, PhD
   - Professor in Product Development and Project-based learning
   - https://www.uia.no/kk/profil/mettemj



********
Contents
********
   
.. toctree::
   :maxdepth: 2



   src/abstract
   src/introduction
   src/objectives
   src/theory
   src/team-contract
   src/development
   src/concept-development
   src/calibration
   src/methods
   src/results
   src/conclusion
   src/discussion
   src/attachments


   src/about/authors
   src/appendices

.. figure:: ./images/straw.jpg
   :scale: 12 %
   :alt: straw1
   :align: center