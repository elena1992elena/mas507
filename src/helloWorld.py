from mas507.msg import ServoSetpoints
import rospy
from JetbotCamera import JetbotCamera

def driveForward():
    print("Hello") 

def changeSpeed(pubSetpoints, setSetpoints, speed):
    setSetpoints.leftWheel  = 319 - speed
    setSetpoints.rightWheel = 322 + speed
    pubSetpoints.publish(servoSetpoints)

def rotateSpeed(pubSetpoints, setSetpoints, speed):
    setSetpoints.leftWheel  = 319 + speed
    setSetpoints.rightWheel = 322 + speed
    pubSetpoints.publish(servoSetpoints)
    
def rideForfard10Cm(pub_servoSetpoints, servoSetpoints):
    changeSpeed(pub_servoSetpoints, servoSetpoints, 20)
    wait(1000)
    changeSpeed(pub_servoSetpoints, servoSetpoints, 0)
    
def rotate45degrees(pub_servoSetpoints, servoSetpoints):
    changeSpeed(pub_servoSetpoints, servoSetpoints, 20)
    wait(1000)
    changeSpeed(pub_servoSetpoints, servoSetpoints, 0)

def takePicture(camera):
    return camera.read()

pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)
servoSetpoints = ServoSetpoints()
jetbotCamera = JetbotCamera()

while True:
    rideForfard10Cm(pub_servoSetpoints, servoSetpoints)
    rotate45degrees(pub_servoSetpoints, servoSetpoints)
    pic = takePicture(jetbotCamera)
    iSee = analysePic(pic)
    if(iSee):
        comeCloser()
    else:
        pnh()