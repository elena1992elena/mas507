import rospy
import time

max_frwrd_left = 89
max_frwrd_right = 92
zero_left = 319
zero_right = 322
max_reverse_left = -115
max_reverse_right = -118
max_up_servo = 306
zero_servo = 510
max_down_servo = -306
first_dist = 0.0325 #25 + 7.5 [cm]. ?????What is the units of first_dist???? 32.5/1000=0.0325????
d_j = 0.011 #distance between wheels on jetbot (radius for turn) 11cm 
r_w = 0.006 #radius of wheels 6cm
r_a = 0.020 #length of arm 20cm
time_to_first = first_dist/(2*np.pi) #If max turning rate is 60 and idstance is 25+7.5 cm [RPM]
time_for_turn = 1/4*r_w #omega is equal to 2pi(IF Max=60[RPM]), deltaphi/deltat=2pir
time_for_pulldown = 1/6*r_a
time_for_pullup = 1/6*r_a #Not totally up (67.5deg)
time_for_pull = 1 # 1 [s]?
time_between_bushes = 2 #time until it sees the strawberry 2?????
strawb_factor_z = strawberryDetection.z*7

#from timer import Timer (if you make a timer.py)
class Timer:
    def __init__(self):
        self._start_time = None

    def start(self):
        """Start a new timer"""
        self._start_time = time.perf_counter()

    def stop(self):
        """Stop the timer, and report the elapsed time"""
        elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None
        #print(f"Elapsed time: {elapsed_time:0.4f} seconds")
#Reference: https://realpython.com/python-timer/


t = Timer()

#1) From: Start of track,  To: First turn
t.stop()
t.start()
while time.perf_counter() < time_to_first:
    servoSetpoints.leftwheel = int(round(zero_left + (max_frwrd_left)))
    servoSetpoints.rightwheel = int(round(zero_right + (max_frwrd_right))
#end

t.stop()
servoSetpoints.leftwheel = zero_left
servoSetpoints.rightwheel = zero_right
#2) From: Start of first turn,  To: End of first turn
t.start()
while time.perf_counter() < time_for_turn:
    servoSetpoints.leftwheel = int(round(zero_left + (max_frwrd_left)))
    servoSetpoints.rightwheel = int(round(zero_right + (0.1*max_reverse_right))
end

t.stop()
#Check for red strawberries
if strawberryDetection.z is None:
    t.start()
    while time.perf_counter() < time_for_turn:
        servoSetpoints.leftwheel = int(round(zero_left + (max_reverse_left)))
        servoSetpoints.rightwheel = int(round(zero_right + (0.1*max_frwrd_right))
    end
    t.stop()
    servoSetpoints.leftwheel = zero_left
    servoSetpoints.rightwheel = zero_right
else:
    #3) From: End of first turn,  To: Bush
    #3a) Pull down arm
    t.start()
    while time.perf_counter() < time_for_pulldown:
        servoSetpoints.servo1 = 510 + int(round((max_down_servo)))
    end
    t.stop()
    #3b) Adjust position towards bush
    while strawberryDetection.z > 0.001:
        old_dist = strawberryDetection.y
        servoSetpoints.leftwheel = int(round(zero_left + (0.5*max_frwrd_left)))
        servoSetpoints.rightwheel = int(round(zero_right + (0.1**max_reverse_right)))
        new_dist = strawberryDetection.y
        if old_dist < new_dist:
            servoSetpoints.leftwheel = int(round(zero_left + (0.1*max_reverse_left)))
            servoSetpoints.rightwheel = int(round(zero_right + (0.5*max_frwrd_right)))
        elif old_dist > new_dist:
            servoSetpoints.leftwheel = int(round(zero_left + (0.5*max_frwrd_left)))
            servoSetpoints.rightwheel = int(round(zero_right + (0.1**max_reverse_right)))
        elif old_dist == new_dist:
            servoSetpoints.leftwheel = int(round(zero_left + (strawb_factor_z*max_frwrd_left)))
            servoSetpoints.rightwheel = int(round(zero_right + (strawb_factor_z*max_frwrd_right)))
        end
    end
    servoSetpoints.leftwheel = zero_left
    servoSetpoints.rightwheel = zero_right
    #4) From: Bush, To: Strawberry
    t.start()
    while time.perf_counter() < time_for_pullup:
        servoSetpoints.servo1 = 510 + int(round((max_up_servo)))
    end
    t.stop()
    servoSetpoints.leftwheel = zero_left
    servoSetpoints.rightwheel = zero_right
    #5) From: Strawberry,  To: Basket
    t.start()
    while time.perf_counter() < time_for_pull:
        servoSetpoints.leftwheel = int(round(zero_left + (0.3*max_reverse_left)))
        servoSetpoints.rightwheel = int(round(zero_right + (0.3*max_reverse_right))
    end
    t.stop()
    servoSetpoints.leftwheel = zero_left
    servoSetpoints.rightwheel = zero_right
    t.start()
    while time.perf_counter() < time_for_back_turn:
        servoSetpoints.leftwheel = int(round(zero_left + (max_reverse_left)))
        servoSetpoints.rightwheel = int(round(zero_right + (0.1*max_frwrd_right))
    end
    t.stop()
    servoSetpoints.leftwheel = zero_left
    servoSetpoints.rightwheel = zero_right
end
#6) From: First Bush,  To: Second Bush
t.start()
while time.perf_counter() < time_between_bushes:
    servoSetpoints.leftwheel = int(round(zero_left + (0.6*max_reverse_left)))
    servoSetpoints.rightwheel = int(round(zero_right + (0.4*max_frwrd_right))
end
t.stop()
servoSetpoints.leftwheel = zero_left
servoSetpoints.rightwheel = zero_right
#Check for red strawberries
#From: Second Bush,  To: Third Bush
#Check for red strawberries
#From: Third Bush,  To: Fourth Bush
#Check for red strawberries
#From: Fourth Bush,  To: 1st ArUco Marker
[INSERT]
#Turn right
[INSERT]
#Drive to 2nd ArUco Marker 
[INSERT]
#Turn right
#From: End of turn,  To: Fourth Bush
#Check for red strawberries
#From: Fourth Bush,  To: Third Bush
#Check for red strawberries
#From: Third Bush,  To: Second Bush
#Check for red strawberries
#From: Second Bush,  To: First Bush
#Check for red strawberries
#Drive to 3nd ArUco Marker and stop
[INSERT]