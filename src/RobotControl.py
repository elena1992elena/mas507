#!/usr/bin/env python
"""
Main Control Node
"""
import rospy
import operator
import numpy as np
import os
from mas507.msg import ServoSetpoints, WebJoystick
from sensor_msgs.msg import Image
from StrawberryDetector import StrawberryDetector
from JetbotCamera import JetbotCamera
from cv_bridge import CvBridge
import cv2
from cv2 import aruco

# Get strawberry function
# 
def getStrawberry(self, msg):
    driveForward()

def driveForward():
    # drive sequence
    print("Hello") 

def takeSnap():
    jetbotCamera = JetbotCamera()
    return jetbotCamera.read()

def lookForArucoMArker():
    rospy.init_node('jetbotCamera')

    # Initilize Jetbot camera instance
    camera = JetbotCamera()

    # CvBridge for converting cv2 to ROS images
    bridge = CvBridge()

    # ROS Image Publishers
    pub_raw = rospy.Publisher('image_raw', Image, queue_size=1)
    pub_calibrated = rospy.Publisher('image_calibrated', Image, queue_size=1)
    pub_markers = rospy.Publisher('image_markers', Image, queue_size=1)
    cv_raw = camera.read()

    # Calibrate raw image
    cv_calibrated = camera.calibrate(cv_raw)

    cv_gray = cv2.cvtColor(cv_calibrated, cv2.COLOR_BGR2GRAY)
    aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
    parameters = aruco.DetectorParameters_create()
    corners, ids, rejectedImgPoints = aruco.detectMarkers(
        cv_gray, aruco_dict, parameters=parameters
    )
    


# Move arm function
# angle - int, rotaion angle in range of () (TODO: you have to find out range)
def moveRobotArm(angle):
    servoSetpoints = ServoSetpoints()
    servoSetpoints.leftWheel  = 319
    servoSetpoints.rightWheel = 322
    # TODO: find out senter position or default position.
    servoSetpoints.servo1  = 319 + angle
    pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)
    pub_servoSetpoints.publish(servoSetpoints)

if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('mainController', anonymous=True)

         # Web Joysticks
        leftJoystick = Joystick()
        rightJoystick = Joystick()

        # Publishers
        pub_strawberry_detection = rospy.Publisher('strawberry_detection', Image, queue_size=1)

        # Strawberry detector
        intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))
        strawberryDetector = StrawberryDetector(pub_strawberry_detection, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])

        # Subscribers
        sub_calibrated = rospy.Subscriber('image_calibrated', Image, strawberryDetector.callback)
        sub_leftJoystick = rospy.Subscriber('webJoystickLeft', WebJoystick, leftJoystick.callback)
        sub_rightJoystick = rospy.Subscriber('webJoystickRight', WebJoystick, rightJoystick.callback)

        # Start Synchronous ROS node execution
        pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)
        t = 0
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            servoSetpoints = ServoSetpoints()
            #print(strawberryDetector.z)
            
            # Range 204..408 
            servoSetpoints.leftWheel  = 319 - rightJoystick.y/2 + rightJoystick.x/7
            servoSetpoints.rightWheel = 322 + rightJoystick.y/2 + rightJoystick.x/7
            # TODO: find out servo and motion range. Use it in moveRobotArm function
            # servoSetpoints.servo1  = 319
            pub_servoSetpoints.publish(servoSetpoints)

            t = t + 0.1

            # Sleep remaining time
            rate.sleep()


        isEndMarker = False
        turnCount = 0
        numberOfTurns = 3

        while operator.not_(isEndMarker):
            # TODO: Your algorithm here
            # moveRobotArm(180)
            snap = takeSnap()

            # strawberryResult - Object with params
            #   isStrawberry - boolean
            #   positionX - int
            #   positionY - int
            #   size - 
            strawberryResult = checkForStrawberry(snap)

            if(strawberryResult.isStrawberry):
                chaseTheStrawberry()

            if(turnCount>= numberOfTurns):
                turnCount = 0
                isEndMarker = returnToInitianCourse()
                continue

            turnTheRobot(Thue)
            turnCount += 1

    except rospy.ROSInterruptException:
        pass