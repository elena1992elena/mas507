#!/usr/bin/env python
import rospy
import numpy as np
import os

import cv2
from cv_bridge import CvBridge

import time
from mas507.msg import ServoSetpoints, ArucoPosition #import all required files (libraries, messages). ArucoPosition is created by us
from sensor_msgs.msg import Image
from StrawberryDetector import StrawberryDetector


class Aruco(object): #create class to save distance to Aruco Marker
    def __init__(self): #to initialise object of the class
        self.x = 0
        self.y = 0
        self.z = 0
        self.xFiltered = 0 #avverage for last 4 measurement
        self.yFiltered = 0
        self.zFiltered = 0
        self.id = 0 #Aruco Marker ID
        self.meanSize = 4 #number of how many of last messurements to remember
        self.lastMeasurements = np.full([self.meanSize,3], 0) #array with last messurements, 4x3 (4 measurements x 3 coordinates (x,y,z))

    def callback(self, aruco): #function to update all the parameters of the object
        self.id = aruco.arucoId

        self.x = aruco.arucoX
        self.y = aruco.arucoY
        self.z = aruco.arucoZ

        self.lastMeasurements = np.roll(self.lastMeasurements, 1, axis=0) #movin all measurements one step down to save new/last measurement
        self.lastMeasurements[0] = np.array([aruco.arucoX, aruco.arucoY, aruco.arucoZ]) 
        
        self.xFiltered = self.filterMeasuredDistanse(0) #calculating filtered measurement for the last 4 measurements
        self.yFiltered = self.filterMeasuredDistanse(1)
        self.zFiltered = self.filterMeasuredDistanse(2)

    def filterMeasuredDistanse(self, index): #function for calculating filtered measurement
        return int(np.mean(self.lastMeasurements[:, index]))

    def __str__(self): #overwrite function to print this object
        return 'x: {}, y: {}, z: {}, xFiltered: {}, yFiltered: {}, zFiltered: {}'.format(self.x, self.y, self.z, self.xFiltered, self.yFiltered, self.zFiltered)


# updateRate = 50.0 # times per second
updateRate = 50.0 # times per second
maxForwardRight = 15 #speed
maxForwardLeft = 15
maxBackwardRight = 15
maxBackwardLeft = 15
turnSpeed = 9
turnSpeedRight = 10
turnSpeedLeft = 10
zeroLeft = 319
zeroRight = 322 

aruco = np.full(10, Aruco()) # aruco library array detection
arucoIdPath = [1, 2, 3] # what aruco Ids to look for on each segment of the path

distanceBetweenBushes = 150 # distance, mm
distanceToStartLine = 0 # distance, mm
distanceFromPathToArucoMarkers = 250 # distance, mm
numberOfBushes = 5 # amount
bushSizeDiameter = 200 # distance, mm
pathClearence = 150 # distance, mm
distanceToMakerLongRun = distanceToStartLine + distanceBetweenBushes * (numberOfBushes + 1) + pathClearence
distanceToMakerShirtRun = bushSizeDiameter + pathClearence * 2

# create array with all distances required to fulfill the run
pathDistanses = [None] * ((numberOfBushes + 1) * 2 + 1)

# distances on the way to aruco marker to reach from start line for zFiltered
pathDistanses[0] = distanceToMakerLongRun - distanceToStartLine - distanceBetweenBushes / 2
# populating other distances
for i in range(1, numberOfBushes):
    pathDistanses[i] = pathDistanses[i-1] - distanceBetweenBushes
pathDistanses[numberOfBushes] = pathDistanses[numberOfBushes-1] - distanceBetweenBushes / 2 - pathClearence

pathDistanses[numberOfBushes + 1] = distanceFromPathToArucoMarkers

for i in range(numberOfBushes + 2, 2 * numberOfBushes + 3):
    pathDistanses[i] = pathDistanses[i - (len(pathDistanses) + 1) / 2 ]
# Done create array with all distances requred to fullfill the run

def ride(servoSetpoints, direction): #function to move the robot. If direction "true" robot going forward, if its "false" - going back
    # print('ride')
    if direction:
        servoSetpoints.leftWheel = int(round(zeroLeft + (maxForwardLeft)))
        servoSetpoints.rightWheel = int(round(zeroRight - (maxForwardRight)))
        # print('going forward')
    else:
        servoSetpoints.leftWheel = int(round(zeroLeft - (maxBackwardLeft)))
        servoSetpoints.rightWheel = int(round(zeroRight + (maxBackwardRight)))
        # print('going back')
    # return servoSetpoints

# speed int number in range 204 and 408
def moveArm(servoSetpoints, position):
    servoSetpoints.servo2 = int(round(position))
    # return servoSetpoints

def turnLeft(servoSetpoints, halvAndBothSpeed): # if "halvAndBothSpeed" is true, robot will turn with a half speed
    # print('turnLeft')
    if(halvAndBothSpeed):
        servoSetpoints.rightWheel = int(round(servoSetpoints.rightWheel - turnSpeedRight/2))
        servoSetpoints.leftWheel = int(round(zeroRight - turnSpeedLeft/2))
    else: 
        servoSetpoints.rightWheel = int(round(servoSetpoints.rightWheel - turnSpeedRight))
    # servoSetpoints.leftWheel = int(round(servoSetpoints.leftWheel - turnSpeedRight))
    # return servoSetpoints

def turnRight(servoSetpoints, halvAndBothSpeed):
    # print('turnRight')
    # servoSetpoints.rightWheel = int(round(servoSetpoints.rightWheel + (turnSpeedRight)))
    if(halvAndBothSpeed):
        servoSetpoints.leftWheel = int(round(zeroRight + turnSpeedLeft/2))
        servoSetpoints.rightWheel = int(round(servoSetpoints.rightWheel + turnSpeedRight/2))
    else: 
        servoSetpoints.leftWheel = int(round(zeroRight + turnSpeedLeft))
    # return servoSetpoints

def stop(servoSetpoints):
    # print('fullStop')
    servoSetpoints.leftWheel = int(round(zeroLeft))
    servoSetpoints.rightWheel = int(round(zeroRight))
    # return servoSetpoints

def myArucoCallback(arucoMarker): #detect what Aruco Marker number is in front of the robot, placing it in corresponding array position, cheking that Aruco Marker number is in range from 0 to 9
    if(arucoMarker.arucoId in range(9)):
        marker = aruco[arucoMarker.arucoId]
        marker.callback(arucoMarker)
        # print(marker)
        aruco[arucoMarker.arucoId] = marker

if __name__ == '__main__':
    try:
        # t = Timer()
        rospy.init_node('mainController', anonymous=True)

        # Robot update rate
        rate = rospy.Rate(updateRate) #how often While loop is updated
        publisher = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1) #creating publisher to send servo positions to the robot

        # Publishers
        pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)
        pub_strawberry_detection = rospy.Publisher('strawberry_detection', Image, queue_size=1) #send strawberry position to the robot
        
        # Strawberry detector
        intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))
        strawberryDetector = StrawberryDetector(pub_strawberry_detection, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])

        # Subscribers
        sub_calibrated = rospy.Subscriber('image_calibrated', Image, strawberryDetector.callback) #to receive strawberry position from the robot
        sub_aruco = rospy.Subscriber('aruco_position', ArucoPosition, myArucoCallback) #to receive distanses to ArucoMarkers
        
        
        print(pathDistanses)
        t = 0
        currentArucoId = 1
        direction = True
        position = 210
#Our Goals:
        # drive to first bush
        goal1IsReached = False
        # turn 90 degrees
        goal2IsReached = False
        # center on the strawberry
        goal3IsReached = False
        # move hand down
        goal4IsReached = False
        # drive hand under the strawberry
        goal5IsReached = False
        # move hand up
        goal6IsReached = False

        start2ndGoal = False
        start3rdGoal = False
        start4thGoal = False
        start5thGoal = False
        start6thGoal = False

        turnRightCounter = 0

        servoSetpoints = ServoSetpoints() #object with servo positions
        servoSetpoints.leftWheel = int(round(zeroLeft))
        servoSetpoints.rightWheel = int(round(zeroRight))
        servoSetpoints.servo2 = int(round(position))
        publisher.publish(servoSetpoints)
#Alghoritm starts here
        while not rospy.is_shutdown():
            # driving to the first bush
            if(goal1IsReached is False):
                if aruco[currentArucoId] is not None: #adjust position of the robot
                    if(aruco[currentArucoId].zFiltered > pathDistanses[0] + 50):
                        ride(servoSetpoints, True)
                    elif(aruco[currentArucoId].zFiltered < pathDistanses[0] - 50):
                        ride(servoSetpoints, False)
                    else:
                        stop(servoSetpoints)
                    if(aruco[currentArucoId].xFiltered > 35):
                        turnRight(servoSetpoints, False)
                    if(aruco[currentArucoId].xFiltered < -35):
                        turnLeft(servoSetpoints, False)

            if(aruco[currentArucoId].zFiltered in range(pathDistanses[0] - 50, pathDistanses[0] + 50) and aruco[currentArucoId].xFiltered in range(-35, 35)): #if robot position is ok we can go to the second goal
                goal1IsReached = True
                start2ndGoal = True
                print('goal1IsReached')

            # turning 90 degrees
            if(start2ndGoal and (goal2IsReached is False)):
                turnRight(servoSetpoints, False)
                turnRightCounter += 1 #robot turns until counter is 550 (observed empirical)
                print(turnRightCounter)
            
            if(turnRightCounter > 550 and start2ndGoal):
                stop(servoSetpoints)
                goal2IsReached = True
                start3rdGoal = True
                print('goal2IsReached')

            # center on the strawberry
            if(start3rdGoal and (goal3IsReached is False)):
                if(strawberryDetector.z > 210): #distance from camera to the stravberry
                    ride(servoSetpoints, True)
                elif(strawberryDetector.z < 190):
                    ride(servoSetpoints, False)
                else:
                    stop(servoSetpoints)
                if(strawberryDetector.x < -50):
                    turnRight(servoSetpoints, True)
                if(strawberryDetector.x > -40):
                    turnLeft(servoSetpoints, True)


            if((190 < strawberryDetector.z < 210) and (-50 < strawberryDetector.x < -40) and start3rdGoal):
                goal3IsReached = True
                start4thGoal = True
                print('goal3IsReached')

            # moving hand down
            if(start4thGoal and (goal4IsReached is False)): 
                moveArm(servoSetpoints, 380)
                goal4IsReached = True
                start5thGoal = True
                print('goal4IsReached')

            # drive hand under the strawberry
            if(start5thGoal and (goal5IsReached is False)):
                if(strawberryDetector.z > 90):
                    ride(servoSetpoints, True)
                elif(strawberryDetector.z < 110):
                    ride(servoSetpoints, False)
                else:
                    stop(servoSetpoints)
                if(strawberryDetector.x < -20):
                    turnRight(servoSetpoints, True)
                if(strawberryDetector.x > -5):
                    turnLeft(servoSetpoints, True)

            if((90 < strawberryDetector.z< 110) and (-20 < strawberryDetector.x < -5) and start5thGoal):
                goal5IsReached = True
                start6thGoal = True

            # move hand up
            if(start6thGoal and (goal6IsReached is False)): 
                moveArm(servoSetpoints, 210)
                goal6IsReached = True

            # if(goal1IsReached and goal2IsReached and goal3IsReached and goal4IsReached and goal5IsReached and goal6IsReached):
            #     break

            ##### Test/play code
            # if(t == 8 * int(updateRate)):
            #     print('move down')
            #     moveArm(servoSetpoints, 380)

            # if(t == 4 * int(updateRate)):
            #     print('move up')
            #     moveArm(servoSetpoints, 210)


            # if direction:
            #     moveArm(publisher, direction, position)
            #     position += 1
            #     if position > 500:
            #         direction = False
            # else:
            #     moveArm(publisher, direction, position)
            #     position -= 1

            ##### Test/play code end

            t += 1
            publisher.publish(servoSetpoints)
            rate.sleep()

    except rospy.ROSInterruptException:
        pass